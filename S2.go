package main

type S2 struct {
	content string
}

func (s S2) ByCopy() int {
	return len(s.content)
}

func (s *S2) ByPointer() int {
	return	len(s.content)
}
