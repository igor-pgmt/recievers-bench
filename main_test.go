package main

import (
	"math/rand"
	"testing"
)

func BenchmarkS1_ByCopy(b *testing.B) {

	tests:= make([]S1, 0)
	for i:=0; i<10; i++ {
		c:=S1{make([]byte, 10000000)}
		rand.Read(c.content)
		tests=append(tests, c)
	}
	for i := 0; i < b.N; i++ {
		for _, tt := range tests {
				tt.ByCopy()
		}
	}
}

func BenchmarkS_ByPointer(b *testing.B) {
	tests:= make([]S1, 0)
	for i:=0; i<10; i++ {
		c:=S1{make([]byte, 10000000)}
		rand.Read(c.content)
		tests=append(tests, c)
	}
	for i := 0; i < b.N; i++ {
		for _, tt := range tests {
				tt.ByPointer()
		}
	}
}


func BenchmarkS2_ByCopy(b *testing.B) {

	tests:= make([]S2, 0)
	for i:=0; i<10; i++ {
		c:=S2{"helloooooooooooooooooooooooo"}
		tests=append(tests, c)
	}
	for i := 0; i < b.N; i++ {
		for _, tt := range tests {
			tt.ByCopy()
		}
	}
}

func BenchmarkS2_ByPointer(b *testing.B) {
	tests:= make([]S2, 0)
	for i:=0; i<10; i++ {
		c:=S2{"helloooooooooooooooooooooooo"}
		tests=append(tests, c)
	}
	for i := 0; i < b.N; i++ {
		for _, tt := range tests {
			tt.ByPointer()
		}
	}
}