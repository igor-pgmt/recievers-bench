package main

type S1 struct {
	content []byte
}

func (s S1) ByCopy() int {
	return len(s.content)
}

func (s *S1) ByPointer() int {
	return	len(s.content)
}